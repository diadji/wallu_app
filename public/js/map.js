/*modules*/
var $ = require("jquery");
var userFunc = require("./user.js");

/*******end modules*********/

/***********VARIABLES********************/
var formCreateUser = $("#form-create-user");
/********************************/
 mapboxgl.accessToken = 'pk.eyJ1Ijoib3N0ZW9uYXBwIiwiYSI6ImNqdjJ3cXdubTBhM3M0NG1wMXc0dXplZ3YifQ.xGnDmwtY6BeLVapcAmJjHQ';
 var map = new mapboxgl.Map({
     container: 'map',
     center: [-14.4392276, 14.5001717],
     zoom: 7,
     style: 'mapbox://styles/mapbox/streets-v11'
 });


$("#btn-create-user").click(() => {
    $("#myModal").css({display : "block"});
})

$(".close").click(() => {
    $("#myModal").css({display : "none"});
})

formCreateUser.submit((event) => {
    event.preventDefault();
    lastName = $("input[name='last-name']").val();
    firstName = $("input[name='first-name']").val();
    login = $("input[name='login']").val();
    password = $("input[name='password']").val();

    var data = {
        lastName:lastName,
        firstName:firstName,
        login:login,
        password:password
    }

    userFunc.createUser(data);
})
