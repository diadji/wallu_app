<?php

namespace App\Model;

class User {
	private $id;
	private $login;
	private $password;
	private $lastName;
	private $firstName;
	private $sexe;
	private $age;
	private $createdAt;
	private $type;

	public function getLogin() {
		return $this->login;
	}

	/**
	 * @param mixed $login
	 *
	 * @return self
	 */
	public function setLogin($login) {
		$this->login = $login;

		return $this;
	}

	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param mixed $password
	 *
	 * @return self
	 */
	public function setPassword($password) {
		$this->password = $password;

		return $this;
	}

	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param mixed $lastName
	 *
	 * @return self
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;

		return $this;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param mixed $firstName
	 *
	 * @return self
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;

		return $this;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 * @param mixed $createdAt
	 *
	 * @return self
	 */
	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getType() {
		return $this->type;
	}

	/**
	 * @param mixed $type
	 *
	 * @return self
	 */
	public function setType($type) {
		$this->type = $type;

		return $this;
	}

	public function getSexe() {
		return $this->sexe;
	}

	/**
	 * @param mixed $sexe
	 *
	 * @return self
	 */
	public function setSexe($sexe) {
		$this->sexe = $sexe;

		return $this;
	}

	public function getAge() {
		return $this->age;
	}

	/**
	 * @param mixed $age
	 *
	 * @return self
	 */
	public function setAge($age) {
		$this->age = $age;

		return $this;
	}

	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return self
	 */
	public function setId($id) {
		$this->id = $id;

		return $this;
	}
}
