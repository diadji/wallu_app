<?php

namespace App\Traits;

use App\Model\User;

trait UserTrait {
	public function getUserInstance($lastName, $firstName, $login, $password) {
		$user = new User();
		$user->setLastName($lastName);
		$user->setFirstName($firstName);
		$user->setLogin($login);
		$user->setPassword($password);

		return $user;
	}
}
