<?php

namespace App\Service;

use App\Model\User;
use Illuminate\Support\Facades\DB;

class UserService {
	public static function create(User $user) {
		return DB::table('user')->insertGetID([
			'last_name' => $user->getLastName(),
			'first_name' => $user->getFirstName(),
			'login' => $user->getLogin(),
			'password' => $user->getPassword(),
		]);
	}
}
