<?php
/**
 *
 */
namespace App\Http\Controllers;

use App\Helper\UserHelper;
use App\Model\User;
use App\Service\UserService;
use Illuminate\Http\Request;

class AppController extends Controller {

	public function viewCreateUser() {

		return view("form-create-user");
	}

	public function createUserAccount(Request $request) {
		$login = $request->input('login');
		$lastName = $request->input("lastName");
		$password = $request->input('password');
		$firstName = $request->input("firstName");

		$userHelper = new UserHelper();
		$result = array();

		$user = new User();

		$user = $userHelper->getUserInstance($lastName, $firstName, $login, $password);

		$result[0] = UserService::create($user);

		return response()->json($result);
	}
}
