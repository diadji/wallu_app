@extends("dashboard")
@section("head")
    <script src="https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.css" rel="stylesheet"/>
@endsection

@section('content')
    <div id="map" style="width: 100%; height: 700px;">
    </div>
    <div class="modal" id="myModal">
        <div id="modal-create-user">
            <span class="close">
                ×
            </span>
            <form id="form-create-user" action="/create-user-account" class="modal-content" method="post">
                <h3>
                    Creation de Compte
                </h3>
                <div class="field">
                    <label>
                        Nom
                    </label>
                    <div class="input-border">
                        <input name="last-name" placeholder="Nom" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="field">
                    <label>
                        Prénom
                    </label>
                    <div class="input-border">
                        <input name="first-name" placeholder="Prénom" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="field">
                    <label>
                        Login
                    </label>
                    <div class="input-border">
                        <input name="login" placeholder="Login" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="field">
                    <label>
                        Mot de Passe
                    </label>
                    <div class="input-border">
                        <input name="password" placeholder="Mot de passe" required="" type="password">
                        </input>
                    </div>
                </div>
                <div class="field-submit">
                    <button id="btn-submit">
                        Créer
                    </button>
                    <img class="small-loader" src="/loaders/loader1.gif" height="100px" width="100px">
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
<script src="/js/main.dist.js" type="text/javascript">
</script>
@endsection
