<!DOCTYPE html>
 <html>
 <head>
    <title>Wallu App</title>
    <link rel="stylesheet" type="text/css" href="/css/dashboard.css">
    <link  href="/css/all.css" rel="stylesheet" type="text/css">
    {{-- <script type="text/javascript" src="/js/assets/jquery.min.js"></script> --}}
    @yield("head")
 </head>
 <body>

    <div id="container">
        <div id="top-bar">
            <h1 id="app-name">Wallu</h1>
        </div>
        <div id="content">
            <div id="nav">
                <div id="menu">
                    <a class="menu-item" href="/">
                        <img class="icon" src="/icons/map-location.svg"/>
                         <div class="item-title">Géolocation</div>
                    </a>

                    <a class="menu-item" id="btn-create-user">
                        <img class="icon" src="/icons/add-user.svg"/>
                        <div class="item-title">Créer Utilisateur</div>
                    </a>
                    <a class="menu-item" href=#>
                        <img class="icon" src="/icons/map-location.svg"/>
                        <div class="item-title">Géolocation</div>
                    </a>
                   <a class="menu-item">
                        <img class="icon" src="/icons/logout.svg"/>
                        <div class="item-title">Se Déconnecter</div>
                    </a>

                </div>
        </div>
        @yield('content')
        </div>
    </div>
    @yield('script')

 </body>
 </html>
