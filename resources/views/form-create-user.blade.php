@extends('dashboard')
@section('head')
<link rel="stylesheet" type="text/css" href="/css/iziModal.min.css">
@endsection
@section('content')
<div id="form-create-user-content">
    <h2>
        Création Compte Utilisateur
    </h2>
    <button id="test">Test</button>
    <div id="myModal" class="modal">
        <div id="form-create-user" >
            <span class="close">&times;</span>
            <form action="/create-user-account" class="modal-content" method="post">
                <h3>Creation de Compte</h3>
                <div class="field">
                    <label>
                        Nom
                    </label>
                    <div class="input-border">
                        <img class="icon" src="/icons/map-location.svg"/>
                        <input name="last-name" placeholder="Nom" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="field">
                    <label>
                        Prénom
                    </label>
                    <div class="input-border">
                        <img class="icon" src="/icons/map-location.svg"/>
                        <input name="first-name" placeholder="Prénom" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="field">
                    <label>
                        Login
                    </label>
                    <div class="input-border">
                        <img class="icon" src="/icons/map-location.svg"/>
                        <input name="login" placeholder="Login" required="" type="text">
                        </input>
                    </div>
                </div>
                <div class="field">
                    <label>
                        Mot de Passe
                    </label>
                    <div class="input-border">
                        <img class="icon" src="/icons/map-location.svg"/>
                        <input name="password" placeholder="Mot de passe" required="" type="password">
                        </input>
                    </div>
                </div>
                <div class="field-submit">
                    <button id="btn-submit">
                        Créer
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="/js/user.dist.js"></script>
@endsection
